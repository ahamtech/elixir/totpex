defmodule TotpexTest do
  use ExUnit.Case

  describe "Generating a code" do
    test "with padded secret" do
      assert Totpex.generate_totp("GE2DIOJYGMZDEMJQGAYDA===", 30, 1_586_240_574) == "280099"
    end

    test "with unpadded secret" do
      assert Totpex.generate_totp("GE2DIOJYGMZDEMJQGAYDA", 30, 1_586_240_574) == "280099"
    end

    test "with invalid secret, raises" do
      assert_raise ArgumentError, fn ->
        Totpex.generate_totp("A1586240574")
      end
    end
  end

  test "Validating a code" do
    assert Totpex.validate_totp(
             "GE2DIOJYGMZDEMJQGAYDA===",
             Totpex.generate_totp("GE2DIOJYGMZDEMJQGAYDA")
           )
  end
end
