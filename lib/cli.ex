defmodule Totpex.CLI do
  @moduledoc false
  def main(_args) do
    totp =
      get_secret()
      |> Totpex.generate_totp()

    IO.puts("Your One-Time Password is #{totp}")
  end

  defp get_secret do
    secret = IO.gets("Please enter your secret key: ") |> String.trim()

    if String.length(secret) > 0 do
      secret
    else
      exit("Please provide only your secret key as argument. Ex: ./totpex qdhu123hsadca")
    end
  end
end
